DOCKER_CMD?=docker
REGISTRY?=shaman
UID?=$(shell id -u)
GID?=$(shell id -g)
TOOLS=cadna shaman standard verrou

.PHONY: all clean sandbox

all: sandbox

clean:
	rm -f $(DEPENDS)

sandbox: Dockerfile $(TOOLS)
	$(DOCKER_CMD) build --build-arg UID=$(UID) --build-arg GID=$(GID) \
	-t $(REGISTRY)/sandbox:latest .

$(TOOLS): %:
	$(DOCKER_CMD) build -t $(REGISTRY)/$@:latest tools/$(subst :,/,$@)


