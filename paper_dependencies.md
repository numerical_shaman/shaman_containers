# Dependencies needed to reproduce the content of the Shaman paper

## State of the art

- [Cadna](https://www-pequan.lip6.fr/cadna/Download_Dir/register.php)
- [Boost interval](https://www.boost.org/doc/libs/1_74_0/libs/numeric/interval/doc/interval.htm)
- MPFR: [MPFR](https://www.mpfr.org/mpfr-current/#download), [GMP](https://gmplib.org/), [mpfrc++](http://www.holoborodko.com/pavel/mpfr/#download)
- [Verrou](https://github.com/edf-hpc/verrou)

## Benchmarks

- [usage example](https://gitlab.com/numerical_shaman/shaman/-/blob/master/examples/sqrtHeron.h)
- [performance benchmarks](https://gitlab.com/nestordemeure/thesis/-/tree/master/code/wrath_of_kahan/performance_benchmarks)
- [LUDecomposition](https://gitlab.com/nestordemeure/thesis/-/tree/master/code/LUdecomposition): [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page), [boost-multiprecision/cpp_dec_float](https://www.boost.org/doc/libs/1_74_0/libs/multiprecision/doc/html/boost_multiprecision/tut/floats/cpp_dec_float.html)
- [Rectangle rule](https://gitlab.com/numerical_shaman/shaman/-/tree/master/examples/summerSchool) (also [here](https://gitlab.com/nestordemeure/thesis/-/tree/master/code/rectangleRule))

## Tools

- [Shaman profiler](https://gitlab.com/numerical_shaman/shaman/-/tree/master/tools/shaman_profiler)
- [Shamanizer](https://gitlab.com/numerical_shaman/shamanizer) (python script version [here](https://gitlab.com/numerical_shaman/shaman/-/tree/master/tools/shamanizer))
