ARG CADNA_INSTALL_PREFIX=/opt/cadna/
ARG SHAMAN_INSTALL_PREFIX=/opt/shaman/
ARG VERROU_INSTALL_PREFIX=/opt/verrou/

FROM shaman/cadna:latest as cadna
FROM shaman/shaman:latest as shaman
FROM shaman/verrou:latest as verrou

# mpfr and libboost interval are from distrib
FROM shaman/standard:latest

LABEL maintener.email=<cedric.chevalier@cea.fr>

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Europe/Paris

# Inherit these args
ARG CADNA_INSTALL_PREFIX
ARG SHAMAN_INSTALL_PREFIX
ARG VERROU_INSTALL_PREFIX

RUN apt-get update && apt-get install --no-install-recommends -y time

COPY --from=cadna $CADNA_INSTALL_PREFIX $CADNA_INSTALL_PREFIX
COPY --from=shaman $SHAMAN_INSTALL_PREFIX $SHAMAN_INSTALL_PREFIX
COPY --from=verrou $VERROU_INSTALL_PREFIX $VERROU_INSTALL_PREFIX

ENV TESTS=$TESTS

ARG UID=1000
ARG GID=1000
ARG HOME=/home/user
ARG TESTS=$HOME/tests
ENV TESTS=$TESTS

RUN addgroup --gid $GID usergroup && \
	useradd -m --uid $UID --groups usergroup --home $HOME user

VOLUME /home/user/tests

USER user

# Make them available during run
ENV CADNA_INSTALL_PREFIX=$CADNA_INSTALL_PREFIX
ENV SHAMAN_INSTALL_PREFIX=$SHAMAN_INSTALL_PREFIX
ENV VERROU_INSTALL_PREFIX=$VERROU_INSTALL_PREFIX

RUN mkdir -p $HOME/tests

CMD ["/bin/bash", "-l"]
