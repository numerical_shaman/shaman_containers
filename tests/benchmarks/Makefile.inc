CXXFLAGS := -std=c++11 -O3 -g -I../tools/include
# -fomit-frame-pointer -march=native -mfpmath=sse -msse3

# lulesh_OMP needs openmp support.
lulesh_OMP:CXXFLAGS+=-fopenmp

TIMER := @time -f "Time : %es (CPU usage : %P, Max Memory usage : %MkB)\n"

.PHONY=all time clean distclean

exe_name ?= nbody mandelbrot spectralnorm lulesh lulesh_OMP

all: $(exe_name)

timers:= $(addsuffix _timer, $(exe_name))

time: $(timers)

ADDITIONAL_ARGS?=
spectralnorm_timer: spectralnorm
	$(TIMER) ./spectralnorm $(ADDITIONAL_ARGS) 5500

nbody_timer: nbody
	$(TIMER) ./nbody $(ADDITIONAL_ARGS) 50000000

mandelbrot_timer: mandelbrot
	$(TIMER) ./mandelbrot $(ADDITIONAL_ARGS) 16000 > mandelbrot.out

lulesh_timer: lulesh
	$(TIMER) ./lulesh $(ADDITIONAL_ARGS) 10

lulesh_OMP_timer: lulesh_OMP
	$(TIMER) ./lulesh_OMP $(ADDITIONAL_ARGS) 10

clean:
	rm -f *.o

distclean: clean
	rm -f *.out $(exe_name)
