
# BENCH :
# - goes in a folder with a default bench_parameters.json file
# - if the folder contains a bench_parameters file, it is used to overwrite a majority of the default file
# - runs cmd_before_bench
# - runs all the target (targets that are not found are ignored)
# - goes into each subfolder with the bench_parameters file (minus the before and after commands)
# - dumps the results as a json file
# - runs cmd_after_bench

import os
import json
import copy
import time
import timeit
import subprocess
from math import sqrt

#--------------------------------------------------------------------------------------------------
# STATISTICS


def mean(data):
    n = float(len(data))
    total = sum(data)
    return total/n


def sd(data):
    n = float(len(data))
    average = mean(data)
    squared_centered_data = map(lambda x: (x-average)*(x-average), data)
    variance = sum(squared_centered_data) / (n-1.)  # bias correction for a sample
    return sqrt(variance)


def median(data):
    n = len(data)
    data = sorted(data)
    index = (n - 1) // 2
    if n % 2:
        return data[index]
    else:
        return (data[index] + data[index + 1])/2.0


def percent(x):
    """produces a text that displays the number in percents"""
    number = round(100*x, 2)
    text = str(number) + '%'
    return text


def summary(data, should_dump_data):
    """returns the minimum, mean, median, maximum and standard deviation of a list of numbers"""
    n = len(data)
    if n == 0:
        return None
    elif n == 1:
        return data[0]
    else:
        result = dict()
        result["sample_size"] = n
        result["min"] = min(data)
        result["max"] = max(data)
        result["mean"] = mean(data)
        result["median"] = median(data)
        result["standard_deviation"] = sd(data)
        result["variation_coefficient"] = percent(result["standard_deviation"] / result["mean"])
        if should_dump_data: result["data"] = data
        return result

#--------------------------------------------------------------------------------------------------
# SHELL COMMAND


def not_empstystring (myString):
    """detects if a string is not blank, empty or None"""
    return bool(myString and myString.strip())


def get_specs():
    """returns a dictionary describing the machine specs"""
    os.environ["LANG"] = 'C.UTF-8'  # sets the environment to english
    spec = subprocess.check_output('lscpu')  # runs a linux command to get machine informations
    spec = spec.split('\n')  # cut in lines
    spec = map(lambda line: line.split(':'), spec)  # cut in [key,value]
    spec = filter(lambda kv: len(kv) == 2, spec)  # keep only lines with a key and a value
    spec = map(lambda kv: (kv[0].strip(), kv[1].strip()),spec)  # builds tuples with the elements and get rid of trailing characters
    spec = dict(spec)  # builds a dictionary with the results
    spec["Hostname"] = subprocess.check_output('hostname').strip()  # adds the machine name
    #spec["MemTotal(RAM)"] = subprocess.check_output('cat /proc/meminfo | grep MemTotal').split(':')[1] # adds the RAM size
    return spec


def build_cmd(prefix, path, executable, arguments):
    """builds a shell command with a prefix, an executable (including its path) and some arguments"""
    cmd = prefix + ' ' + path + '/' + executable
    cmd = reduce(lambda res, arg: res + ' ' + str(arg), arguments, cmd)
    return cmd


def run_cmd(cmd):
    """runs a command, displays a warning if it might require a source"""
    if not_empstystring(cmd):
        print("COMMAND : " + cmd)
        os.system(cmd)
        if "source" in cmd:
            print("WARNING : The call might contains a `source` command, you need to run source by yourself before running this script.")

#--------------------------------------------------------------------------------------------------
# FILE MANIPULATION


def get_timestring():
    """produces a string with the current time expressed as the date and hour in minutes"""
    return time.strftime("%d/%m/%Y %Hh%M", time.localtime())


def get_timestamp():
    """produces a timestamp"""
    return time.strftime("%s", time.localtime())


def get_bench_parameters(parameters, folder_name, path):
    """drops the before/after cmd, adds local parameters if a file is detected"""
    parameters = copy.deepcopy(parameters)
    parameters["cmd_before_bench"] = ""
    parameters["cmd_after_bench"] = ""
    file_path = path + '/' + parameters_file
    try:  # there is a parameter file in the folder
        with open(file_path, 'r') as file:
            print("BENCHMARK : " + parameters_file + " file detected in " + folder_name + " folder.")
            for key, value in json.load(file).iteritems():
                parameters[key] = value
            return parameters
    except IOError:  # there is no parameter file in the folder
        print("BENCHMARK : No " + parameters_file + " file detected in " + folder_name + " folder.")
        return parameters


def get_subfolders(path):
    """returns a list of subfolders for the given path"""
    return [path for path in os.listdir(path) if os.path.isdir(path)]


def export_dict(machine_specs, include_time_stamp, path, folder_name, dict):
    """Exports a dictionnary as a json file"""
    if len(dict) != 0:
        file_path = path + '/' + folder_name
        if include_time_stamp: file_path = file_path + '_' + get_timestamp()
        file_path = file_path + "_bench.json"
        with open(file_path, 'w') as file:
            output = {"Data": dict, "Machine_specs": machine_specs, "Date": get_timestring()}
            json.dump(output, file, indent=3, encoding="utf-8", sort_keys=True)

#--------------------------------------------------------------------------------------------------
# BENCHMARKING


parameters_file = "bench_parameters.json"


def timer(cmd, sample_size, should_dump_data):
    """times a given executable"""
    timer = timeit.Timer(lambda: os.system(cmd)) # we need to call cmd with os.system for Valgrind types of calls
    times = timer.repeat(repeat=sample_size, number=1)
    return summary(times, should_dump_data)


def bench_executable(executable, arguments, parameters, path):
    """Runs a benchmark on a given executable"""
    print("BENCHMARK : Benching " + executable)
    cmd = build_cmd(parameters["cmd_prefix"], path, executable, arguments)
    print("COMMAND : " + cmd)
    time = timer(cmd,parameters["sample_size"], parameters["dump_data"])
    print("RESULT : {0}".format(time))
    return time


def bench_folder(default_parameters, path, folder_name):
    """runs a benchmark in a given folder with some default parameters"""
    print("BENCHMARK : Running " + folder_name + " bench.")
    # loading the parameters
    parameters = get_bench_parameters(default_parameters, folder_name, path)
    run_cmd(parameters["cmd_before_bench"])
    result = dict()
    # running the benchmark on each target in the folder
    for executable, arguments in parameters["targets"].iteritems():
        if os.path.isfile(path + '/' + executable):
            result[executable] = bench_executable(executable, arguments, parameters, path)
        else: print("BENCHMARK : The " + executable + " target was not found in " + folder_name + '.')
    # running the benchmark in each subfolder
    for subfolder_path in get_subfolders(path):
        subfolder_name = os.path.basename(subfolder_path)
        result[subfolder_name] = bench_folder(parameters, subfolder_path, subfolder_name)
    # outputing the results
    export_dict(parameters["machine_specs"], parameters["time_stamp"], path, folder_name, result)
    run_cmd(parameters["cmd_after_bench"])
    return result

#--------------------------------------------------------------------------------------------------
# MAIN


# produces a set of default parameters including the specs of the machine
default_parameters = {
    "sample_size": 1,  # how many time should we run each executable
    "dump_data": False,  # should we dump the raw data with the statistical summary ?
    "time_stamp": False,  # should we include a timestamp in the output files name ?
    "cmd_prefix": "",  # concatenated in front of the executable names
    "cmd_before_bench": "",  # ran when entering the folder (not transmitted to the subfolders)
    "cmd_after_bench": "",  # ran when leaving the folder (not transmitted to the subfolders)
    "machine_specs": get_specs(),  # description of the current machine
    "targets": {}  # {executable:[args]}
}

# runs the bench function in the current folder
path = "."
bench_folder(default_parameters, path, "full")
