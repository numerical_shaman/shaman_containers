
# Performance Benchmarks

## Usage

To run the benchmark, compile the codes you are interested in and run `python bench.py`. The script will produce json files with the results and machine specs.

You can modify the `bench_parameters.json` file to set the benchmarks parameters, you can also create a file with the same name in subfolders in order to specify folder specific parameters.

## Benchmarks

### the Computeur Benchmarks Game

[The Computeur Language Benchmarks Game](https://benchmarksgame.alioth.debian.org/) is a well known benchmarking suite that is used to compare the peak performances of programming languages on different tasks.
We decided to instrument the three tasks that dealt with floating point arithmetic:

- [n-body](http://benchmarksgame.alioth.debian.org/u64q/nbody-description.html#nbody) ([Implementation](http://benchmarksgame.alioth.debian.org/u64q/program.php?test=nbody&lang=gpp&id=1)): Double-precision N-body simulation.

- [Spectral norm](http://benchmarksgame.alioth.debian.org/u64q/spectralnorm-description.html#spectralnorm) ([Implementation](http://benchmarksgame.alioth.debian.org/u64q/program.php?test=spectralnorm&lang=gpp&id=1)): Computing eigenvalue using the power method.

- [Mandelbrot](http://benchmarksgame.alioth.debian.org/u64q/mandelbrot-description.html#mandelbrot) ([Implementation](http://benchmarksgame.alioth.debian.org/u64q/program.php?test=spectralnorm&lang=gpp&id=1)): Generating the Mandelbrot set at a given resolution, it is the only parallel benchmark of the three. 

The implementations we instrumented are the quickest C++ implementations for each problem (at the time of writing) that do not rely on explicit vectorization or calls to library (such as Eigen) in order to do their computations.

As the code is highly optimized (several orders of magnitude faster than a naive implementation) we expect to observe extreme slowdown.

### Lulesh 1.0

[Livermore Unstructured Lagrangian Explicit Shock Hydrodynamics](https://codesign.llnl.gov/lulesh.php) is a proxy application that is widely used as a performance benchmark in exascale computing. 
We instrumented the serial version of the code to compare different tool on an application that is representative of the kind of computations that are actually done in high performance computing (solving explicit hydrodynamics equations on a collection of volumetric elements).

While our tests on the computer benchmark game were about finding applications that would exhibit exceptional slowdown, with Lulesh we expect to see results that are consistent with the overhead we observe in most numerical applications.

![](https://codesign.llnl.gov/images/sedov-3d-LLNL.png?raw=true)

## Potential improvement

- Adding an option to desable some bench or libraries

