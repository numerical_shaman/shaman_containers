
#ifndef BOOST_NUMERIC_INTERVAL_COMPARE_HANDMADE_HPP
#define BOOST_NUMERIC_INTERVAL_COMPARE_HANDMADE_HPP

#include <boost/numeric/interval.hpp>

template<class T, class Policies> inline
T safeMedian(const boost::numeric::interval<T, Policies>& x)
{
    if(empty(x))
    {
        return 0;
    }
    else
    {
        return (upper(x) + lower(x)) / 2;
    }
};

template<class T, class Policies1, class Policies2> inline
bool operator<(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) < safeMedian(y);
}

template<class T, class Policies> inline
bool operator<(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) < y;
}

template<class T, class Policies1, class Policies2> inline
bool operator<=(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) <= safeMedian(y);
}

template<class T, class Policies> inline
bool operator<=(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) <= y;
}

template<class T, class Policies1, class Policies2> inline
bool operator>(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) > safeMedian(y);
}

template<class T, class Policies> inline
bool operator>(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) > y;
}

template<class T, class Policies1, class Policies2> inline
bool operator>=(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) >= safeMedian(y);
}

template<class T, class Policies> inline
bool operator>=(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) >= y;
}

template<class T, class Policies1, class Policies2> inline
bool operator==(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) == safeMedian(y);
}

template<class T, class Policies> inline
bool operator==(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) == y;
}

template<class T, class Policies1, class Policies2> inline
bool operator!=(const boost::numeric::interval<T, Policies1>& x, const boost::numeric::interval<T, Policies2>& y)
{
    return safeMedian(x) != safeMedian(y);
}

template<class T, class Policies> inline
bool operator!=(const boost::numeric::interval<T, Policies>& x, const T& y)
{
    return safeMedian(x) != y;
}

#endif // BOOST_NUMERIC_INTERVAL_COMPARE_HANDMADE_HPP