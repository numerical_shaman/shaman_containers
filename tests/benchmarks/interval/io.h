//
// Created by demeuren on 24/05/18.
//

#ifndef WRATH_OF_KAHAN_IO_H
#define WRATH_OF_KAHAN_IO_H

#include <boost/numeric/interval.hpp>

template<class T, class Policies, class CharType, class CharTraits>
std::basic_ostream <CharType, CharTraits> &operator<<
        (std::basic_ostream <CharType, CharTraits> &stream,
         const boost::numeric::interval <T, Policies> &value)
{
    if (empty(value))
    {
        return stream << "[]";
    }
    else
    {
        return stream << '[' << lower(value) << ',' << upper(value) << ']';
    }
}

#endif //WRATH_OF_KAHAN_IO_H
