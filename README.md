# shaman_containers

Containers for comparing tools

## Building containers

We use `docker` or `docker` compatible command line and file definitions. So it can work also with `podman`.

To build container, simply run (`DOCKER_BUILDKIT=1` is optional but recommanded if presents on your system)
```shell
DOCKER_BUILDKIT=1 make
```

It currently build 5 containers:
- `shaman/cadna`, which contains a `Cadna` installation,
- `shaman/shaman`, which contains a `Shaman` installation,
- `shaman/verrou`, which contains a `Verrou` installation,
- `shaman/standard`, which contains a `ubuntu-20.04` installed with `boost-interval` and `mpfr`,
- `shaman/sandbox`, which is built on top of the previous containers and allows to experiment with all these tools.

## Running benchmarks

Once containers are built and `docker` is [started](https://stackoverflow.com/a/33596140/6422174), they can be used like this (which `bash` compatible shell):
```bash
docker run -v $(pwd)/tests/benchmarks:/home/user/tests -it shaman/sandbox

cd ~/tests
make
make time
```

